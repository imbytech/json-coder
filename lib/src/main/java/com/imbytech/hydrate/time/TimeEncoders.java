package com.imbytech.hydrate.time;

import java.time.ZoneId;
import java.time.Instant;
import java.time.DayOfWeek;
import java.time.Month;
import java.time.temporal.TemporalAmount;
import java.time.temporal.Temporal;
import com.imbytech.hydrate.Encoder;
import com.imbytech.hydrate.StringEncoder;
import com.imbytech.hydrate.NumberEncoder;
import com.imbytech.hydrate.EnumEncoder;

/**
 * Set of encoders to handle date and time values stored in JSON as numbers
 * or ISO-8601 strings.
 */
public class TimeEncoders {
    /**
     * Create a JSON encoder that encodes a time zone value as a string.
     *
     * @return an encoder that can encode a time zone to a JSON string
     */
    public static Encoder<ZoneId> zoneIdEncoder() {
        return new StringEncoder().map(v -> v.toString());
    }

    /**
     * Create a JSON encoder that encodes an instant as a number, given an
     * instant precision.
     *
     * @param precision the instant precision to use
     * @return an encoder that can encode an instant to a JSON number
     */
    public static Encoder<Instant> instantEncoder(InstantPrecision precision) {
        return new NumberEncoder().map(v -> precision.toEpoch(v));
    }

    /**
     * Create a JSON encoder that encodes a temporal to a string.
     *
     * @return an encoder that can encode a temporal to a JSON string
     */
    public static Encoder<Temporal> temporalEncoder() {
        return new StringEncoder().map(v -> v.toString());
    }

    /**
     * Create a JSON encoder that encodes a temporal amount to a string.
     *
     * @return an encoder that can encode a temporal amount to a JSON string
     */
    public static Encoder<TemporalAmount> temporalAmountEncoder() {
        return new StringEncoder().map(v -> v.toString());
    }

    /**
     * Create a JSON encoder that encodes a day of week to a string.
     *
     * @return an encoder that can encode a day of week to a JSON string
     */
    public static Encoder<DayOfWeek> dayOfWeekEncoder(boolean toLowercase) {
        return new EnumEncoder<DayOfWeek>(toLowercase);
    }

    /**
     * Create a JSON encoder that encodes a month to a string.
     *
     * @return an encoder that can encode a month to a JSON string
     */
    public static Encoder<Month> monthEncoder(boolean toLowercase) {
        return new EnumEncoder<Month>(toLowercase);
    }
}