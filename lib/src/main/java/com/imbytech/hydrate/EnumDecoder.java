package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class EnumDecoder<E extends Enum<E>> extends JsonPrimitiveDecoder<E> {
    private final Class<E> type;
    private StringDecoder decoder;

    public EnumDecoder(Class<E> type) {
        super();
        this.type = type;
        this.decoder = new StringDecoder(true);
    }

    public E decodePrimitive(JsonPrimitive primitive) {
        String value = decoder.decodePrimitive(primitive);
        E result = null;
        if(value != null) {
            try {
                result = Enum.valueOf(type, value);
            } catch(IllegalArgumentException iae1) {
                try {
                    result = Enum.valueOf(type, value.toUpperCase());
                } catch(IllegalArgumentException iae2) {
                    // do nothing, keep it null
                }
            }
        }
        return result;
    }

    public static <T extends Enum<T>> EnumDecoder<T> of(Class<T> type) {
        return new EnumDecoder<T>(type);
    }
}
