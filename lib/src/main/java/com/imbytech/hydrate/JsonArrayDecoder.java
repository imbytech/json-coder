package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;

public abstract class JsonArrayDecoder<T> extends Decoder<T> {
    public T decode(JsonElement element) {
        if(element != null && element.isJsonArray()) {
            return decodeArray(element.getAsJsonArray());
        } else {
            return null;
        }
    }

    public abstract T decodeArray(JsonArray array);
}
