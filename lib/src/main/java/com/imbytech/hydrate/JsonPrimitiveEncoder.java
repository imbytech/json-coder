package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonNull;

public abstract class JsonPrimitiveEncoder<T> extends Encoder<T> {
    public JsonElement encode(T input) {
        if(input == null) {
            return JsonNull.INSTANCE;
        } else {
            return encodePrimitive(input);
        }
    }

    public abstract JsonPrimitive encodePrimitive(T input);
}