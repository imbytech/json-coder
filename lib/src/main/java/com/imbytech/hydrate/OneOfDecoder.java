package com.imbytech.hydrate;

import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;
import com.google.gson.JsonElement;

public class OneOfDecoder<T> extends Decoder<T> {
    private List<Function<JsonElement, ? extends T>> funs;

    @SafeVarargs
    public OneOfDecoder(Function<JsonElement, ? extends T>... funs) {
        this.funs = new ArrayList<Function<JsonElement, ? extends T>>();
        for(Function<JsonElement, ? extends T> fun : funs) {
            this.funs.add(fun);
        }
    }

    public T decode(JsonElement element) {
        T result = null;
        for(Function<JsonElement, ? extends T> fun : funs) {
            result = fun.apply(element);
            if(result != null) {
                break;
            }
        }
        return result;
    }
}
