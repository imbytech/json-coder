package com.imbytech.hydrate;

import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ObjectEncoder<T> extends Encoder<T> {
    private Map<String,Function<? super T,JsonElement>> fields;

    private ObjectEncoder(Map<String,Function<? super T,JsonElement>> fields) {
        this.fields = fields;
    }

    public ObjectEncoder() {
        this(new HashMap<String,Function<? super T,JsonElement>>());
    }

    public <V> void putField(String name, Function<? super T,? extends V> getter, Encoder<V> encoder) {
        fields.put(name, encoder.map(getter));
    }

    public <V> ObjectEncoder<T> withField(String name, Function<? super T,? extends V> getter, Encoder<V> encoder) {
        ObjectEncoder<T> result = new ObjectEncoder<T>(this.fields);
        result.putField(name, getter, encoder);
        return result;
    }

    public JsonElement encode(T input) {
        return encodeToObject(input);
    }

    public JsonObject encodeToObject(T input) {
        JsonObject result = new JsonObject();
        for(Map.Entry<String,Function<? super T,JsonElement>> field : fields.entrySet()) {
            result.add(field.getKey(), field.getValue().apply(input));
        }
        return result;
    }
}
