package com.imbytech.hydrate;

import java.util.List;
import java.util.stream.Collectors;

public class Decoders {
    public static Decoder<Integer> intDecoder() {
        return new NumberDecoder().map(v -> v.intValue());
    }

    public static Decoder<Long> longDecoder() {
        return new NumberDecoder().map(v -> v.longValue());
    }

    public static Decoder<Float> floatDecoder() {
        return new NumberDecoder().map(v -> v.floatValue());
    }

    public static Decoder<Double> doubleDecoder() {
        return new NumberDecoder().map(v -> v.doubleValue());
    }

    public static <T> Decoder<List<T>> listDecoder(Decoder<T> decoder) {
        return new StreamDecoder<T>(decoder).map(
            s -> s.collect(Collectors.toList())
            );
    }
}
