package com.imbytech.hydrate;

import java.util.List;
import java.util.ArrayList;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.function.BiConsumer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ObjectDecoder<T> extends JsonObjectDecoder<T> {
    private Supplier<? extends T> constructor;
    private List<BiFunction<? super T,JsonElement,? extends T>> fields;

    private ObjectDecoder(Supplier<? extends T> constructor, List<BiFunction<? super T,JsonElement,? extends T>> fields) {
        this.constructor = constructor;
        this.fields = fields;
    }

    public ObjectDecoder(Supplier<? extends T> constructor) {
        this(constructor, new ArrayList<BiFunction<? super T,JsonElement,? extends T>>());
    }

    public <V> void addField(BiConsumer<? super T,? super V> setter, Decoder<V> decoder) {
        fields.add(new FieldSetter<T,V>(setter, decoder));
    }

    public <V> void addField(String name, BiConsumer<? super T,? super V> setter, Decoder<V> decoder) {
        addField(setter, new FieldDecoder<V>(name, decoder));
    }

    public <V> ObjectDecoder<T> withField(String name, BiConsumer<? super T,? super V> setter, Decoder<V> decoder) {
        ObjectDecoder<T> result = new ObjectDecoder<T>(this.constructor, this.fields);
        result.addField(name, setter, decoder);
        return result;
    }

    public <V> ObjectDecoder<T> withField(BiConsumer<? super T,? super V> setter, Decoder<V> decoder) {
        ObjectDecoder<T> result = new ObjectDecoder<T>(this.constructor, this.fields);
        result.addField(setter, decoder);
        return result;
    }

    public T decodeObject(JsonObject element) {
        T result = constructor.get();
        for(BiFunction<? super T,JsonElement,? extends T> field : fields) {
            result = field.apply(result, element);
        }
        return result;
    }

    private static class FieldSetter<T,V> implements BiFunction<T,JsonElement,T> {
        private BiConsumer<? super T,? super V> setter;
        private Decoder<V> decoder;

        public FieldSetter(BiConsumer<? super T,? super V> setter, Decoder<V> decoder) {
            this.setter = setter;
            this.decoder = decoder;
        }

        public T apply(T object, JsonElement element) {
            Decoder<T> objDecoder = this.decoder.map(v -> {
                setter.accept(object, v);
                return object;
            });
            return objDecoder.decode(element);
        }
    }
}
