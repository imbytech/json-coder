package com.imbytech.hydrate;

import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class MapDecoder<T> extends JsonObjectDecoder<Map<String,T>> {
    private Function<JsonElement,? extends T> valueDecoder;

    public MapDecoder(Function<JsonElement,? extends T> valueDecoder) {
        this.valueDecoder = valueDecoder;
    }

    public Map<String,T> decodeObject(JsonObject obj) {
        Map<String,T> result = new HashMap<String,T>();
        for(Map.Entry<String,JsonElement> entry : obj.entrySet()) {
            result.put(entry.getKey(), this.valueDecoder.apply(entry.getValue()));
        }
        return result;
    }
}