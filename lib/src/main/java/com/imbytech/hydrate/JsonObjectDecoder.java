package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class JsonObjectDecoder<T> extends Decoder<T> {
    public T decode(JsonElement element) {
        if(element != null && element.isJsonObject()) {
            return decodeObject(element.getAsJsonObject());
        } else {
            return null;
        }
    }

    public abstract T decodeObject(JsonObject object);
}
