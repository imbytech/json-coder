package com.imbytech.hydrate;

import java.util.Map;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonObject;

public class MapEncoderTest {
    @Test
    public void testEncodeStrings() {
        Map<String,String> map = new HashMap<String,String>();
        map.put("monday", "lundi");
        map.put("tuesday", "mardi");
        map.put("wednesday", "mercredi");
        MapEncoder<String> encoder = new MapEncoder<String>(new StringEncoder());
        JsonObject result = encoder.encodeObject(map);
        assertEquals("lundi", result.get("monday").getAsString());
        assertEquals("mardi", result.get("tuesday").getAsString());
        assertEquals("mercredi", result.get("wednesday").getAsString());
    }
}
