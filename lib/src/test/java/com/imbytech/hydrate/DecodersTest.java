package com.imbytech.hydrate;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonArray;

public class DecodersTest {

    // Numbers

    @Test
    public void testLong() {
        JsonPrimitive primitive = new JsonPrimitive(123456L);
        Decoder<Long> decoder = Decoders.longDecoder();
        long result = decoder.decode(primitive);
        assertEquals(123456L, result);
    }

    @Test
    public void testDouble() {
        JsonPrimitive primitive = new JsonPrimitive(123.45);
        Decoder<Double> decoder = Decoders.doubleDecoder();
        double result = decoder.decode(primitive);
        assertEquals(123.45, result, 0.001);
    }

    // List

    @Test
    public void testList() {
        JsonArray array = new JsonArray();
        array.add("hello");
        array.add("world");
        Decoder<List<String>> decoder = Decoders.listDecoder(new StringDecoder());
        List<String> result = decoder.decode(array);
        assertThat(
            new String[]{"hello", "world"},
            is(result.toArray())
            );
    }
}
