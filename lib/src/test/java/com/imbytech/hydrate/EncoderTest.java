package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.google.gson.JsonElement;

public class EncoderTest {
    @Test
    public void testMap() {
        Encoder<String> encoder = new NumberEncoder().map(s -> s.length());
        JsonElement result = encoder.encode("I am a string");
        assertTrue(result.isJsonPrimitive());
        assertEquals("I am a string".length(), result.getAsNumber());
    }

    @Test
    public void testWithDefault() {
        Encoder<String> encoder = new StringEncoder().withDefault("empty");
        JsonElement result = encoder.encode(null);
        assertTrue(result.isJsonPrimitive());
        assertEquals("empty", result.getAsString());
    }

    @Test
    public void testWithDefaultNotTriggered() {
        Encoder<String> encoder = new StringEncoder().withDefault("empty");
        JsonElement result = encoder.encode("I am not empty");
        assertTrue(result.isJsonPrimitive());
        assertEquals("I am not empty", result.getAsString());
    }
}
